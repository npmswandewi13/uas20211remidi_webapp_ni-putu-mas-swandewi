from django.shortcuts import render, redirect
from API import models
from django.contrib import messages
from django.http import HttpResponseRedirect
from datetime import datetime
from django.core.paginator import Paginator
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
