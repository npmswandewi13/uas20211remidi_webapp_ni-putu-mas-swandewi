from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.ConnectStrava)
admin.site.register(models.MyActivity)
admin.site.register(models.MyTeam)
admin.site.register(models.Sport)