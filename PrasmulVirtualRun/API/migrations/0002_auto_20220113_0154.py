# Generated by Django 3.2 on 2022-01-12 18:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('API', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='myactivity',
            old_name='email',
            new_name='username',
        ),
        migrations.AlterField(
            model_name='myactivity',
            name='activity',
            field=models.ForeignKey(max_length=35, on_delete=django.db.models.deletion.CASCADE, to='API.connectstrava'),
        ),
    ]
