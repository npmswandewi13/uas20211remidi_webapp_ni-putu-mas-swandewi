from django.db import models
# from Accounts import models as account_models
# from Accounts.models import CustomUser
import datetime
from django.conf import settings
from django.db.models.aggregates import Max

# Create your models here.
class ConnectStrava(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    status_type = (
        ('Connected','Connected'),
        ('Un-Connected','Un-Connected')
    )
    status = models.CharField(max_length=25, choices=status_type)
    def __str__(self):
        return self.user

class Sport(models.Model):
    sport_name = models.CharField(max_length=50)
    sport_type = (
        ('Normal','Normal'),
        ('Extreme','Extreme'),
    )
    sport_type = models.CharField(max_length=25, choices=sport_type, default='Normal')
    def __str__(self):
        return self.sport_name

class MyActivity(models.Model):
    username = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=False)
    activity = models.ForeignKey(Sport, max_length=35, on_delete=models.CASCADE)
    date_time = models.DateTimeField(default=datetime.datetime.now)
    title = models.CharField(max_length=50)
    distance = models.IntegerField()
    elevation = models.IntegerField()
    type_list = (
        ('Commute','Commute'),
        ('Private','Private')
    )
    type = models.CharField(max_length=20, choices=type_list, default='Private')
    def __str__(self):
        return '{} - {}'.format(self.activity, self.title)

class MyTeam(models.Model):
    email = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    team = models.CharField(max_length=30)
    # activity = models.ForeignKey(MyActivity, choices=activity_type)
    date_time = models.DateTimeField(default=datetime.datetime.now)
    title = models.CharField(max_length=50)
    distance = models.IntegerField()
    elevation = models.IntegerField()
